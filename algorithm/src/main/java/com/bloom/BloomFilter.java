package com.bloom;

import cn.hutool.core.util.HashUtil;
import com.sun.istack.internal.NotNull;

import java.util.concurrent.Executors;

/**
 * 布隆过滤器
 * 数据结构:一个大型的位数组和几个不一样的无偏 hash 函数
 * 预计元素的数量 n，错误率 f，位数组的长度 l，hash 函数的最佳数量 k
 * k=0.7*(l/n) # 约等于
 * f=0.6185^(l/n) # ^ 表示次方计算，也就是 math.pow
 * @ClassName BloomFilter
 * @Author 19012511
 * @Date 2021/7/15 15:39
 **/
public class BloomFilter {

    //错误率
    private Double f = 0.01;
    //预计元素的数量,估计的过大，会浪费存储空间，估计的过小，就会影响准确率
    private Integer n = 100;
    //位数组的长度
    private Integer l;
    //hash 函数的最佳数量
    private Integer k;

    private byte[] bloomCache;

    private String[] seeds = new String[]{"d","a","s","b"};

    public BloomFilter() {

        this.bloomCache = new byte[this.n.intValue()];
    }

    public BloomFilter(@NotNull Double f, @NotNull Integer n) {
        this.f = f;
        this.n = n;
        this.bloomCache = new byte[this.n.intValue()];
    }

    private int hash(String v, String salt){
        int hash = Math.abs(HashUtil.javaDefaultHash(v + salt));
        return hash = hash % n;
    }

    public void add(String v){
        for (String seed : seeds) {
            int hash = hash(v, seed);
            bloomCache[hash] = 1;
        }
    }

    public boolean exists(String v){
        for (String seed : seeds) {
            int hash = hash(v, seed);
            if (1 != bloomCache[hash]){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        BloomFilter bloomFilter = new BloomFilter();
        for (int i = 0; i< 100; i++){
            bloomFilter.add("user"+i);
        }
        for (int i = 0; i< 100; i++){
            if (!bloomFilter.exists("user"+i)){
                System.out.println(i);
            }
        }
    }
}
