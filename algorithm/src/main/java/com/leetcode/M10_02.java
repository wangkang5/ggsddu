package com.leetcode;

import org.apache.commons.compress.utils.Lists;

import java.util.*;

public class M10_02 {

    public List<List<String>> groupAnagrams(String[] strs) {
        HashMap<String, List<String>> map = new HashMap<>();

        for (String str : strs) {
            char[] bytes = str.toCharArray();
            Arrays.sort(bytes);
            String s = new String(bytes);

            if (map.containsKey(s)){
                map.get(s).add(str);
            } else {
                List<String> list = new ArrayList<>();
                list.add(str);
                map.put(s,list);
            }
        }
        return new ArrayList<List<String>>(map.values());

    }

    public static void main(String[] args) {
        M10_02 m10_02 = new M10_02();
        m10_02.groupAnagrams(new String[]{"eat","tea","tan","ate","nat","bat"});
    }
}
