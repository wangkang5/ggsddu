package com.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @ClassName N1104
 * @Author 19012511
 * @Date 2021/7/29 9:29
 **/
public class N1104 {
    public List<Integer> pathInZigZagTree(int label) {
        List<Integer> res = new ArrayList<>();
        res.add(label);
        if (1 == label){
            return res;
        }
        int i = label;
        int c = 0;
        //计算在第几层
        while (0 != i) {
            i = i >> 1;
            c++;
        }
        //
        int begin = 1 << (c - 1);
        int end = (1 << c) - 1;
        boolean direct = 1 == c % 2 ? true : false;
        int pos = 0;
        if (direct) {
            pos = label - begin + 1;
        } else {
            pos = end - label + 1;
        }

        c--;
        while (c > 1) {
            pos = (pos+ 1) >> 1 ;
            System.out.println(pos);
            begin = 1 << (c - 1);
            end = (1 << c) - 1;
            direct = 1 == c % 2 ? true : false;
            if (direct) {
                res.add(pos+begin-1);
            } else {
                res.add(end-pos+1);
            }
            c--;
        }
        res.add(1);
        Collections.reverse(res);
        return res;
    }

    public static void main(String[] args) {
        N1104 n1104 = new N1104();
        System.out.println(n1104.pathInZigZagTree(26));

    }
}
