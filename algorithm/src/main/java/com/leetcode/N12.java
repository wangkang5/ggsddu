package com.leetcode;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class N12 {
    public String intToRoman(int num) {
        Map<Integer,String> map = new LinkedHashMap<>();
        map.put(1000,"M");
        map.put(900,"CM");
        map.put(500,"D");
        map.put(400,"CD");
        map.put(100,"C");
        map.put(90,"XC");
        map.put(50,"L");
        map.put(40,"XL");
        map.put(10,"X");
        map.put(9,"IX");
        map.put(5,"V");
        map.put(4,"IV");
        map.put(1,"I");
        String s = "";
        while (num != 0){
            for (Map.Entry<Integer, String> m: map.entrySet()){
                if (num >= m.getKey()){
                    s += m.getValue();
                    num -= m.getKey();
                    break;
                }
            }
        }

        return s;
    }

    public static void main(String[] args) {
        N12 n12 = new N12();
        System.out.println(n12.intToRoman(20));

    }
}
