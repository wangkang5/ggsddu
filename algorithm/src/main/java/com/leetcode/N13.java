package com.leetcode;

import java.util.HashMap;

public class N13 {
    public int romanToInt(String s) {
        int res=0;
        int temp=0;
        for (int i=0; i<s.length(); i++){
            int num = getnumber(s.charAt(i));
            if (num>temp){
                res = res+num-temp-temp;
            } else {
                res+=num;
            }
            temp = num;
        }
        return res;

    }

    public int getnumber(char c){
        switch (c){
            case 'I':
                return 1;
            case 'V':
                return 5;
            case 'X':
                return 10;
            case 'L':
                return 50;
            case 'C':
                return 100;
            case 'D':
                return 500;
            case 'M':
                return 1000;
            default:
                return 0;
        }
    }
}
