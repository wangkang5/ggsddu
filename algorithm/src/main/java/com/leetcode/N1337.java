package com.leetcode;

import java.util.PriorityQueue;
import java.util.Queue;

public class N1337 {
    public int[] kWeakestRows(int[][] mat, int k) {
        PriorityQueue<int[]> queue = new PriorityQueue<>((a,b)->{//police,row
            if (a[0] != b[0]){
                return b[0] - a[0];
            }
            return b[1] - a[1];
        });
        for (int i = 0; i<mat.length; i++){
            int l = 0;
            int r = mat[i].length-1;
            while (l<r){
                int mid = (r+l)>>1;
                System.out.println(mid);
                if (mat[i][mid] == 1){
                    l = mid+1;
                } else {
                    r = mid;
                }
            }
            if (mat[i][r] == 1){
                queue.add(new int[]{r+1,i});
            } else {
                queue.add(new int[]{r,i});
            }
        }
        int[] res = new int[k];
        for (int i = 0; i< k; i++){
            res[i] = queue.poll()[1];
        }
        return res;

    }
}
