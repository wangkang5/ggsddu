package com.leetcode;

import java.util.HashMap;

/**
 * @ClassName N138
 * @Author 19012511
 * @Date 2021/7/22 13:51
 **/
class Node {
    int val;
    Node next;
    Node random;

    public Node(int val) {
        this.val = val;
        this.next = null;
        this.random = null;
    }
}
public class N138 {
    public Node copyRandomList(Node head) {
        HashMap<Node, Node> map = new HashMap<>();
        Node node = head;
        while (node != null){
            Node clone = new Node(node.val);
            map.put(node,clone);
            node = node.next;
        }
        node = head;
        while (node !=null){
            map.get(node).next = map.get(node.next);
            map.get(node).random = map.get(node.random);
            node = node.next;
        }
        return map.get(head);
    }
}
