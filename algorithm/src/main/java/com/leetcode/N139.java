package com.leetcode;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @ClassName N139
 * @Author 19012511
 * @Date 2021/7/20 16:02
 **/
public class N139 {
    public boolean wordBreak(String s, List<String> wordDict) {
        Set<String> wordDictSet = new HashSet(wordDict);
        boolean[] dp = new boolean[s.length() + 1];
        dp[0] = true;

        for (int i = 1; i< s.length(); i++){
            for (int j = 0; j < i; j++){
                if (dp[j] && wordDictSet.contains(s.substring(j,i))){
                    dp[i] = true;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println("abc".substring(0,1));
    }
}
