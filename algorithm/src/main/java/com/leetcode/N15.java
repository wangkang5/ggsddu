package com.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName N15
 * @Author 19012511
 * @Date 2021/7/12 16:14
 **/
public class N15 {
    public List<List<Integer>> threeSum(int[] nums) {
        List resList = new ArrayList<>();
        if (null == nums || nums.length<=2){
            return resList;
        }
        Arrays.sort(nums);
        int len = nums.length;
        for (int i=0; i< len-2; i++){
            if (nums[i]>0){
                return resList;
            }
            System.out.println(resList.size());
            int l = i+1;
            int r = len-1;
            while (l<r){
                if (nums[i] + nums[l] + nums[r] == 0) {
                    resList.add(Arrays.asList(nums[i], nums[l], nums[r]));
                    while (nums[l] == nums[l + 1] && l < r-1){
                        l++;
                    }
                    while (nums[r] == nums[r - 1] && l+1 < r){
                        r--;
                    }
                    l++;
                    r--;
                } else if (nums[i] + nums[l] + nums[r] < 0){
                    l++;
                } else {
                    r--;
                }
            }
            while (nums[i] == nums[i + 1] && i < len - 2) {
                i++;
            }
        }
        return resList;
    }

    public static void main(String[] args) {
        N15 n15 = new N15();
        System.out.println(n15.threeSum(new int[]{-1,0,1,2,-1,-4}));

    }
}
