package com.leetcode;

import java.util.Arrays;

/**
 * @ClassName N16
 * @Author 19012511
 * @Date 2021/7/14 19:22
 **/
public class N16 {
    public int threeSumClosest(int[] nums, int target) {
        Arrays.sort(nums);
        int n = nums.length;
        int res = 0;
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < n - 2; i++) {
            int l = i + 1;
            int r = n - 1;
            while (l < r) {
                int temp = nums[i] + nums[l] + nums[r];
                if (temp - target == 0) {
                    return target;
                } else if (temp - target > 0) {
                    r--;
                } else {
                    l++;
                }
                if (Math.abs(temp - target) < min) {
                    min = Math.abs(temp - target);
                    res = temp;
                }
            }
        }
        return res;
    }
}
