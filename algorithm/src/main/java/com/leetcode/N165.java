package com.leetcode;

import com.sun.deploy.util.StringUtils;
import org.apache.poi.util.StringUtil;

/**
 * @ClassName N165
 * @Author 19012511
 * @Date 2021/7/27 10:40
 **/
public class N165 {
    public int compareVersion(String version1, String version2) {
        String[] v1 = version1.split("\\.");
        String[] v2 = version2.split("\\.");

        int len = Math.max(v1.length,v2.length);

        int[] vv1 = new int[len];
        int[] vv2 = new int[len];
        for (int i = 0; i< len; i++){
            if (i<v1.length){
                vv1[i] = s2i(v1[i]);
            }
            if (i<v2.length){
                vv2[i] = s2i(v2[i]);
            }
            if (vv1[i] == vv2[i]){
                continue;
            } else {
                return vv1[i] > vv2[i] ? 1 : -1;
            }
        }
        return 0;
    }

    private int s2i(String s){
        while (s.length()>0 && s.charAt(0) == 0){
            s = s.substring(1);
        }
        if ("".equals(s)){
            return 0;
        } else {
            return Integer.valueOf(s);
        }
    }

    public static void main(String[] args) {
        System.out.println(Integer.valueOf("+001"));
        System.out.println("1bs".substring(1));
    }
}
