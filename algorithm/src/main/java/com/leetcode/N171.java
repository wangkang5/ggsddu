package com.leetcode;

/**
 * @ClassName N171
 * @Author 19012511
 * @Date 2021/7/30 14:32
 **/
public class N171 {
    public int titleToNumber(String columnTitle) {
        int n = columnTitle.length();
        int res = 0;
        for (int i = n-1; i>=0; i--){
            res += (columnTitle.charAt(n-i-1)-'A'+1) * Math.pow(26d,i);
        }
        return res;
    }

    public static void main(String[] args) {
        N171 n171 = new N171();
        System.out.println(n171.titleToNumber("ZY"));
    }
}
