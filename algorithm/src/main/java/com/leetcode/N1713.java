package com.leetcode;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName N1713
 * @Author 19012511
 * @Date 2021/7/26 10:45
 **/
public class N1713 {

    public int minOperations(int[] t, int[] arr) {
        int n = t.length, m = arr.length;
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            map.put(t[i], i);
        }
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            int x = arr[i];
            if (map.containsKey(x)) list.add(map.get(x));
        }
        int len = list.size();
        System.out.println(list);
        if (0 == len){
            return n;
        }
        int max = 1;
        int[] dp = new int[len];
        dp[0] = 1;
        for (int i = 1; i< len; i++){
            dp[i] = 1;
            for (int j = 0; j< i; j++){
                if (list.get(i)> list.get(j)){
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            max = Math.max(max, dp[i]);
        }
        System.out.println(Arrays.toString(dp));
        return n - max;

    }

    public static void main(String[] args) throws Exception{
//        N1713 n1713 = new N1713();
//        System.out.println(n1713.minOperations(
//                new int[]{19,15,2,3,10,6,7,4,8,14},
//                new int[]{9,7,9,2,15,14,3,8,14,8}));
    }
}
