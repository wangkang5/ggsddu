package com.leetcode;

public class N1736 {
    public String maximumTime(String time) {

        String res = "";
        for (int i = 0; i < time.length(); i++) {

            if (i == 0) {
                if ('?' == time.charAt(i)) {
                    if (Character.isDigit(time.charAt(i+1)) && time.charAt(i+1)-'0'>4){
                        res += "1";
                    } else {
                        res += "2";
                    }

                } else {
                    res += time.charAt(i);
                }
            } else if (1 == i) {
                if ('?' == time.charAt(i)) {
                    if ('2' == res.charAt(i - 1)) {
                        res += "3";
                    } else {
                        res += "9";
                    }
                } else {
                    res += time.charAt(i);
                }
            } else if (2 == i) {
                res += time.charAt(i);
            } else if (3 == i) {
                if ('?' == time.charAt(i)) {
                    res += "5";
                } else {
                    res += time.charAt(i);
                }
            } else if (4 == i) {
                if ('?' == time.charAt(i)) {
                    res += "9";
                } else {
                    res += time.charAt(i);
                }
            }
        }
        return res;

    }
}
