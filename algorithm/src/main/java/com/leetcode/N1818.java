package com.leetcode;

import sun.awt.Mutex;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @ClassName N1818
 * @Author 19012511
 * @Date 2021/7/14 9:31
 **/
public class N1818 {
    public int minAbsoluteSumDiff(int[] nums1, int[] nums2) {
        int[] nums = nums1.clone();
        Arrays.sort(nums);
        double what = 1E9 + 7;
        long maxCha = 0;
        long res = 0;
        for (int i = 0; i < nums1.length; i++) {
            int tt = Math.abs(nums1[i]-nums2[i]);
            res += tt;
//            for (int j = 0; j < nums1.length; j++) {
//                int ttt = Math.abs(nums1[j]-nums2[i]);
//                if (tt-ttt>maxCha){
//                    maxCha = tt-ttt;
//                }
//            }
            int l = 0, r = nums.length - 1;
            while (l < r) {
                int mid = l + r + 1 >> 1;
                if (nums[mid] <= nums2[i]) l = mid;
                else r = mid - 1;
            }
            int ttt = Math.abs(nums[r] - nums2[i]);
            if (r + 1 < nums.length) ttt = Math.min(ttt, Math.abs(nums[r + 1] - nums2[i]));
            if (ttt < tt) maxCha = Math.max(maxCha, tt - ttt);

        }
        System.out.println(res);
        System.out.println(maxCha);
        res = (long)((res - maxCha)% what);
        return (int)res;
    }

    public int minAbsoluteSumDiff1(int[] nums1, int[] nums2) {
        int maxCha = 0;
        int res = 0;
        for (int i = 0; i < nums1.length; i++) {
            res += Math.abs(nums1[i]-nums2[i]);
            for (int j = 0; j < nums1.length; j++) {
                if ((nums1[i] >= nums1[j] && nums1[i] <= nums2[j]) || (nums1[i] <= nums1[j] && nums1[i] >= nums2[j])) {
                    maxCha = Math.max(maxCha,Math.abs(nums1[i]-nums1[j]));
                }
            }
        }
        res = res - maxCha;
        return res;
    }
}
