package com.leetcode;

import java.util.Arrays;

/**
 * @ClassName N1846
 * @Author 19012511
 * @Date 2021/7/15 9:19
 **/
public class N1846 {
    public int maximumElementAfterDecrementingAndRearranging(int[] arr) {
        Arrays.sort(arr);
        if (arr[0] > 1) arr[0] = 1;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] != arr[i-1]){
                arr[i] = arr[i-1]+1;
            }
        }
        return arr[arr.length-1];
    }
}
