package com.leetcode;

import java.util.Arrays;

/**
 * @ClassName N1877
 * @Author 19012511
 * @Date 2021/7/20 9:14
 **/
public class N1877 {
    public int minPairSum(int[] nums) {
        Arrays.sort(nums);
        int l = 0, r = nums.length - 1;
        int res = Integer.MIN_VALUE;
        while (l < r) {
            res = Math.max(nums[l] + nums[r], res);
            l++;
            r--;
        }
        return res;
    }
}
