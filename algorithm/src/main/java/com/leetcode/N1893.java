package com.leetcode;

import java.util.HashMap;
import java.util.TreeMap;

/**
 * @ClassName N1893
 * @Author 19012511
 * @Date 2021/7/23 9:36
 **/
public class N1893 {
    public boolean isCovered(int[][] ranges, int left, int right) {
        loop: for (int i = left; i <= right; i++ ){
            for (int[] range : ranges){
                if (range[0] <= i && range[1]>=i){
                    continue loop;
                }
            }
            return false;
        }
        return true;


    }
}
