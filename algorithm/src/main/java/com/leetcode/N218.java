package com.leetcode;

import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * @ClassName N218
 * @Author 19012511
 * @Date 2021/7/13 9:24
 **/
public class N218 {
    public List<List<Integer>> getSkyline(int[][] buildings) {
        List<List<Integer>> ans = new ArrayList<>();

        // 预处理所有的点，为了方便排序，对于左端点，令高度为负；对于右端点令高度为正
        List<int[]> ps = new ArrayList<>();
        for (int[] b : buildings) {
            int l = b[0], r = b[1], h = b[2];
            ps.add(new int[]{l, -h});
            ps.add(new int[]{r, h});
        }
        Collections.sort(ps,(a,b)->{
            if (a[0]!=b[0]){
                return a[0]-b[0];
            } else {
                return a[1]-b[1];
            }
        });
        PriorityQueue<Integer> max = new PriorityQueue<>((a, b) -> b-a);
        int last = 0;
        max.add(last);
        for (int[] p : ps) {
            if (p[1] < 0){
                max.add(-p[1]);
            } else {
                max.remove(p[1]);
            }
            int cur = max.peek();
            if (last != cur){
                ans.add(Arrays.asList(p[0],cur));
                last = cur;
            }
        }
        return ans;
    }

    public static int recursion(int i) throws Exception{
        Thread.sleep(100);
        System.out.println(i);
        while (i<8){
            i++;
            return recursion(i);
        }
        return i;
    }

    public static void recursion1(int i) throws Exception{
        Thread.sleep(100);
        System.out.println(i);
        while (i<4){
            i++;
            recursion1(i);
            return;
        }
    }

    public static int facttail(int n, int a) throws Exception {
        if (n < 0)
            return 0;
        else if (n == 0)
            return 1;
        else if (n == 1)
            return a;
        else
            return facttail(n - 1, n * a);
    }

    public static void main(String[] args) throws Exception {
        recursion1(0);
//        System.out.println(facttail(3, 1));
    }
}
