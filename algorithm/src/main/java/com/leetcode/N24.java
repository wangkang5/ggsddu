package com.leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @ClassName N24
 * @Author 19012511
 * @Date 2021/7/21 19:10
 **/
class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}
public class N24 {


    public ListNode swapPairs(ListNode head) {
        if (null == head || null == head.next){
            return head;
        }
        ListNode res = head.next;
        ListNode cur = head;
        while (null != cur){
            ListNode first = cur;
            ListNode second = cur.next;
            ListNode third = cur.next.next;

            second.next = first;
            if (null != third && null != third.next){
                first.next = third.next;
            } else {
                first.next = third;
                break;
            }
            cur = third;
        }
        return res;
    }

    public static void main(String[] args) {
//        N24 n24 = new N24();
//        ListNode listNode1 = new ListNode(1);
//        ListNode listNode2 = new ListNode(2);
//        ListNode listNode3 = new ListNode(3);
//        ListNode listNode4 = new ListNode(4);
//        listNode1.next = listNode2;
//        listNode2.next = listNode3;
//        listNode3.next = listNode4;
//        System.out.println(n24.swapPairs(listNode1));


    }
}
