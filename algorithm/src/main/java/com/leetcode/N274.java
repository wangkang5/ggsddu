package com.leetcode;

import java.util.Arrays;

public class N274 {

    public int hIndex(int[] citations) {
        int len = citations.length;
        int count = 0,cc = 0;
        for (int h = len; h>=1 ; h --){
            count = 0;
            cc = 0;
            for (int i = 0; i<len; i++){
                if (citations[i]>=h ){
                    count++;
                } else{
                    cc ++;
                }
            }
            if (h == count){
                return h;
            }
            System.out.println(h);
        }
        return Math.min(count,cc);

    }

    public int hIndex1(int[] citations) {
        Arrays.sort(citations);
        for (int i = 0; i < citations.length; i++) {
            int h = citations.length - i;
            if (h <= citations[i]) {
                return h;
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        N274 n275 = new N274();
        System.out.println(n275.hIndex(new int[]{1,3,1}));


    }
}
