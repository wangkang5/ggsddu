package com.leetcode;

/**
 * @ClassName N275
 * @Author 19012511
 * @Date 2021/7/12 16:03
 **/
public class N275 {
    public int hIndex(int[] citations) {
        int len = citations.length;
        int i;
        for (i = len; i > 0; i--) {
            if (i <= citations[len-i]) {
                break;
            }
        }
        return i;
    }
}
