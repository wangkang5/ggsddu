package com.leetcode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.PriorityQueue;

/**
 * @ClassName N313
 * @Author 19012511
 * @Date 2021/8/9 14:49
 **/
public class N313 {
    public int nthSuperUglyNumber1(int n, int[] primes) {
        int i = 0;
        int ans = 1;
        lloop:while (n != 0) {
            ans = ++i;
            int temp = ans;
            System.out.println("current temp:"+temp);
            loop:while (temp != 1) {
                for (int prime : primes) {
                    if (0 == temp % prime) {
                        temp = temp / prime;
                        continue loop;
                    }
                }
                continue lloop;
            }
            n--;
        }
        return ans;
    }
    public int nthSuperUglyNumber(int n, int[] primes) {
        PriorityQueue<Long> queue = new PriorityQueue<>();
        HashSet<Long> set = new HashSet<>();
        set.add(1L);
        queue.offer(1L);
        Long ans = 1L;
        while (n>0){
            n--;
            ans = queue.poll();
            for (int prime : primes) {
                long temp = prime * ans;
                if (set.add(temp)){
                    queue.offer(temp);
                }
            }
        }
        return ans.intValue();
    }

    public static void main(String[] args) {
        N313 n313 = new N313();
        System.out.println(n313.nthSuperUglyNumber(12,new int[]{2,7,13,19}));
    }
}
