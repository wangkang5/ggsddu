package com.leetcode;

import org.apache.commons.codec.binary.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName N345
 * @Author 19012511
 * @Date 2021/8/19 16:09
 **/
public class N345 {
    public String reverseVowels(String s) {
        if (s.length() == 1){
            return s;
        }
        int l = 0,r = s.length()-1;
        List<Character> list = new ArrayList<>();
        list.add('A');
        list.add('E');
        list.add('I');
        list.add('O');
        list.add('U');
        list.add('a');
        list.add('e');
        list.add('i');
        list.add('o');
        list.add('u');
        StringBuilder sss = new StringBuilder(s);
        StringBuilder end = new StringBuilder();
        while (l<r){
            if (list.contains(s.charAt(l)) && list.contains(s.charAt(r))){
                sss.replace(l,l+1,String.valueOf(s.charAt(r)));
                sss.replace(r,r+1,String.valueOf(s.charAt(l)));
                l++;
                r--;
            } else if (list.contains(s.charAt(l))){
//                end.append(s.charAt(r));
                r--;
            } else if (list.contains(s.charAt(r))){
//                start.append(s.charAt(l));
                l++;
            } else {
//                start.append(s.charAt(l));
//                end.append(s.charAt(r));
                l++;
                r--;
            }
        }
        return sss.toString();
    }
}
