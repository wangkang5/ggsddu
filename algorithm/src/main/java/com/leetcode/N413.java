package com.leetcode;

/**
 * @ClassName N413
 * @Author 19012511
 * @Date 2021/8/10 15:06
 **/
public class N413 {
    public int numberOfArithmeticSlices(int[] nums) {
        if (nums.length<3){
            return 0;
        }
        int cha = nums[1] - nums[0];
        int threeCount = 0;
        int ans = 0;
        for (int i = 2; i < nums.length; i++) {
            int curCha = nums[i] - nums[i - 1];
            if (cha == curCha){
                threeCount++;
                ans += threeCount;
            } else {
                threeCount = 0;
                cha = curCha;
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        N413 n413 = new N413();
        System.out.println(n413.numberOfArithmeticSlices(new int[]{1,2,3,4}));
    }
}
