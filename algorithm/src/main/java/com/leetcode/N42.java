package com.leetcode;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * @ClassName N42
 * @Author 19012511
 * @Date 2021/7/13 16:02
 **/
public class N42 {
    public int trap(int[] height) {
        int res = 0;
        int[] newHeight = new int[height.length + 2];
        for (int i = 0; i < height.length; i++) {
            newHeight[i + 1] = height[i];
        }
        Stack<Integer> lst = new Stack<>();//递增栈
        Stack<Integer> rst = new Stack<>();//递减栈
        for (int i = 1; i < newHeight.length; i++) {
            if (lst.isEmpty() || newHeight[lst.peek()] <= newHeight[i]) {
                lst.add(i);
            }
        }
        for (int i = newHeight.length - 2; i >= 0; i--) {
            if (rst.isEmpty() || newHeight[rst.peek()] < newHeight[i]) {
                rst.add(i);
            }
        }
        while (lst.size() > 1) {
            Integer pop = lst.pop();
            if (pop - 1 != lst.peek()) {
                int temp = newHeight[lst.peek()] * (pop - lst.peek()-1);
                for (int n = lst.peek() + 1; n < pop; n++) {
                    temp -= newHeight[n];
                }
                res += temp;
            }
        }
        while (rst.size() > 1) {
            Integer pop = rst.pop();
            if (pop + 1 != rst.peek()) {
                int temp = newHeight[rst.peek()] * (rst.peek() - pop -1);
                for (int n = pop + 1; n < rst.peek(); n++) {
                    temp -= newHeight[n];
                }
                res += temp;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        N42 n42 = new N42();
        System.out.println(n42.trap(new int[]{0,1,0,2,1,0,1,3,2,1,2,1}));

    }
}
