package com.leetcode;

import org.apache.commons.codec.binary.StringUtils;

/**
 * @ClassName N516
 * @Author 19012511
 * @Date 2021/8/12 16:52
 **/
public class N516 {
    public int longestPalindromeSubseq(String s) {
        int n = s.length();
        int[][] dp = new int[n + 1][n + 1];
        for (int i = 0; i < s.length(); i++) {
            for (int j = 0; j < s.length(); j++) {
                dp[i+1][j+1] = Math.max(Math.max(dp[i+1][j],dp[i][j+1]),dp[i][j]);
                if (s.charAt(i) == s.charAt(n-j-1)){
                    dp[i+1][j+1] += 1;
                }
            }
        }
        return dp[n][n];
    }

    public static void main(String[] args) {
        N516 n516 = new N516();
        System.out.println(n516.longestPalindromeSubseq("bbbab"));
    }
}
