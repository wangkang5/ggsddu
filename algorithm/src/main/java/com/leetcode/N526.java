package com.leetcode;

import java.util.ArrayList;

/**
 * @ClassName N526
 * @Author 19012511
 * @Date 2021/8/16 19:22
 **/
public class N526 {
    public int countArrangement(int n) {
        ArrayList<Integer> array = new ArrayList<>();
        boolean[] vis = new boolean[n + 1];
        boolean f = true;
        while (f) {
            int cur = array.size() + 1;
            int count = 0;
            for (int i = 1; i <= n; i++) {
                if (!vis[n] && (0 == i % cur || 0 == cur % i)) {
                    vis[n] = true;
                    count++;
                }
            }
            System.out.println("cur:" + cur + "count:" + count);
            if (0 == count) {
                f = false;
            } else {
                array.add(count);
            }
        }
        if (array.size() == 0) {
            return 0;
        }
        int ans = 1;
        for (Integer integer : array) {
            ans = ans * integer;
        }
        return ans;
    }
}
