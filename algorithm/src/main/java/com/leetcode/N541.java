package com.leetcode;

/**
 * @ClassName N541
 * @Author 19012511
 * @Date 2021/8/20 9:27
 **/
public class N541 {
    public String reverseStr(String s, int k) {
        char[] chars = s.toCharArray();

        int cur = 0;
        while (cur*k<chars.length){
            int l = cur*k;
            int r = Math.min(l + k -1 ,chars.length-1);
            if (0 == cur%2){
                while (l<r){
                    char lChar = chars[l];
                    chars[l] = chars[r];
                    chars[r] = lChar;
                    l++;
                    r--;
                }
            }
            cur++;
        }
        return new String(chars);
    }

    public static void main(String[] args) {
        new N541().reverseStr("abcdefg",2);
    }

}
