package com.leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @ClassName N547
 * @Author 19012511
 * @Date 2021/7/20 10:53
 **/
public class N547 {

    //BFS 创建一个队里，pop第一个，add它的所有邻居，遍历直到满足条件或者队里为空
    //read:是否遍历过
    //queue:存放未遍历邻居
    public int findCircleNum(int[][] isConnected) {
        int len = isConnected.length;
        List<Integer> read = new ArrayList<>();
        int res = 0;
        while (read.size() < len){
            res++;
//            System.out.println(res);
            LinkedList<Integer> queue = new LinkedList<>();
            for (int i = 0; i< len; i++){
                if (!read.contains(i)){
                    queue.add(i);
                    read.add(i);
                    break;
                }
            }
            while (queue.size()!=0){
                Integer pop = queue.pop();
//                System.out.println("pop:"+pop);
                List<Integer> linju = readLinju(isConnected, read, pop);
                queue.addAll(linju);
                read.addAll(linju);
            }
        }
        return res;
    }

    private List<Integer> readLinju(int[][] isConnected, List<Integer> read, int n){
        List<Integer> r = new ArrayList<>();
        for (int i = 0; i< isConnected.length ;i++){
            if (isConnected[n][i] == 1 && !read.contains(i)){
                r.add(i);
            }
        }
        return r;
    }

    //DFS 遍历第一个，递归它的下一个直到尽头
    //visited:是否遍历过
    public int findCircleNum1(int[][] isConnected){
        int len = isConnected.length;
        boolean[] visited = new boolean[len];
        int res = 0;
        for (int i = 0; i < len; i++) {
            if (!visited[i]) {
                dfs(isConnected, visited, len, i);
                res++;
            }
        }
        return res;
    }

    private void dfs(int[][] isConnected, boolean[] visited, int len, int i){
        for (int j = 0; j < len; j++) {
            if (isConnected[i][j] == 1 && !visited[j]) {
                visited[j] = true;
                dfs(isConnected, visited, len, j);
            }
        }
    }
    //并查集
    public int findCircleNum3(int[][] isConnected){
        int len = isConnected.length;
        //初始化,自己是自己根节点
        int fa[] = new int[len];
        int rank[] = new int[len];
        for (int i=0; i<len; i++){
            fa[i] = i;
            rank[i] = 1;
        }

        for (int i = 0; i < len; i++) {
            for (int j = i + 1; j < len; j++) {
                if (isConnected[i][j] == 1) {
                    union(rank,fa , i, j);
                }
            }
        }
        int res = 0;
        for (int i = 0; i < len; i++) {
            if (fa[i] == i) {
                res++;
            }
        }
        return res;
    }

    private int find(int[] fa, int x) {
        if (fa[x] == x) {
            //根节点是自己，返回根节点
            return x;
        } else {
            //递归查找父节点的根节点
            //find方法里路径压缩，将沿途节点的父节点设置为根
            fa[x] = find(fa, fa[x]);
            return fa[x];
        }
    }

    private void union(int[] rank, int[] fa, int x, int y) {
        int xx = find(fa, x);
        int yy = find(fa, y);
        //按秩合并，将简单的树往复杂树上合并
        if (rank[xx] >= rank[yy]) {
            fa[yy] = xx;
        } else {
            fa[xx] = yy;
        }
        if (rank[xx] == rank[yy] && xx != yy) {
            rank[xx]++;
        }
    }

    public static void main(String[] args) {
        N547 n547 = new N547();
        System.out.println(n547.findCircleNum(new int[][]{{1,1,0},{1,1,0},{0,0,1}}));
    }
}
