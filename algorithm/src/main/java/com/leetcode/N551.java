package com.leetcode;

/**
 * @ClassName N551
 * @Author 19012511
 * @Date 2021/8/19 16:34
 **/
public class N551 {
    public boolean checkRecord(String s) {
        boolean flag = true;
        int A = 0;
        int L = 0;
        for (int i = 0; i < s.length(); i++) {
            if ('A' == s.charAt(i)) {
                A++;
            }
            if ('L' == s.charAt(i)) {
                L++;
            } else {
                L = 0;
            }
            if (A >= 2 || L >= 3) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    public static void main(String[] args) {
        new N551().checkRecord("LPPLPP");
    }
}
