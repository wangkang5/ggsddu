package com.leetcode;

/**
 * @ClassName N552
 * @Author 19012511
 * @Date 2021/8/19 17:30
 **/
public class N552 {
    int mod = (int) 1e9 + 7;

    public int checkRecord(int n) {
        int[][][] dp = new int[n + 1][2][3];
        dp[0][0][0] = 1;
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j < 2; j++) { // A: 0,1
                for (int k = 0; k < 3; k++) { // L: 0,1,2
                    if (j == 1 && k == 0) { // A
                        dp[i][1][0] = (dp[i][j][k] + dp[i - 1][0][0]) % mod;
                        dp[i][1][0] = (dp[i][j][k] + dp[i - 1][0][1]) % mod;
                        dp[i][1][0] = (dp[i][j][k] + dp[i - 1][0][2]) % mod;
                    }
                    if (k != 0) { // L
                        dp[i][j][k] = (dp[i][j][k] + dp[i - 1][j][k - 1]) % mod;
                    }
                    if (k == 0) { // P
                        dp[i][j][0] = (dp[i][j][0] + dp[i - 1][j][0]) % mod;
                        dp[i][j][0] = (dp[i][j][0] + dp[i - 1][j][1]) % mod;
                        dp[i][j][0] = (dp[i][j][0] + dp[i - 1][j][2]) % mod;
                    }
                }
            }
        }
        int ans = 0;
        for (int j = 0; j < 2; j++) { // A: 0,1
            for (int k = 0; k < 3; k++) { // L: 0,1,2
                ans = (ans + dp[n][j][k]) % mod;
            }
        }
        return ans;
    }
}
