package com.leetcode;

import java.io.*;
import java.util.*;

/**
 * @ClassName N581
 * @Author 19012511
 * @Date 2021/8/3 12:43
 **/
public class N581 {
    private int ll;
    public int findUnsortedSubarray(int[] nums) {
        int[] sort = nums.clone();
        Arrays.sort(sort);
        int zheng = 0;
        int fan = 0;
        for (int i = 0 ; i< nums.length; i++) {
            if (sort[i] == nums[i]){
                zheng++;
            } else {
                break;
            }
        }
        if (nums.length == zheng){
            return 0;
        }
        for (int i = nums.length-1 ; i>=0; i--) {
            if (sort[i] == nums[i]){
                zheng++;
            } else {
                break;
            }
        }
        return nums.length-zheng;
    }

    public static void main(String[] args) {
        System.out.println(Integer.class.getClassLoader());
        System.out.println(N581.class.getClassLoader());
    }
}
