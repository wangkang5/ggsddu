package com.leetcode;

import java.util.Arrays;

/**
 * @ClassName N611
 * @Author 19012511
 * @Date 2021/8/4 15:15
 **/
public class N611 {
    public int triangleNumber(int[] nums) {
        Arrays.sort(nums);
        int length = nums.length;
        int ans = 0;
        for (int i = 0; i < length - 2; i++) {
            for (int j = i + 1; j < length - 1; j++) {
                int max = nums[i] + nums[j];
                int min = Math.abs(nums[i]- nums[j]);
                for (int k = j+1; k<length; k++){
                    if (min<nums[k] && max>nums[k]){
                        System.out.println(i+":"+j+":"+k);
                        ans++;
                    }
                }
            }
        }
        return ans;
    }
}
