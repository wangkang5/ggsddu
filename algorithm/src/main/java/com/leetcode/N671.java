package com.leetcode;

import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;

/**
 * @ClassName N671
 * @Author 19012511
 * @Date 2021/7/27 8:58
 **/
class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
public class N671 {
    int ans = -1;
    public int findSecondMinimumValue(TreeNode root) {
        dfs(root,root.val);
        return ans;
    }
    void dfs(TreeNode root, int cur) {
        if (root == null) return;
        if (root.val != cur){
            if (ans == -1) ans = root.val;
            else ans = Math.min(ans,root.val);
            return;
        }
        dfs(root.left,cur);
        dfs(root.right,cur);
    }


    public static void main(String[] args) {

    }
}
