package com.leetcode;

import java.util.*;

/**
 * @ClassName N743
 * @Author 19012511
 * @Date 2021/8/2 14:11
 **/
public class N743 {
    public int networkDelayTime(int[][] times, int n, int k) {
        Map<Integer, List<int[]>> map = new HashMap<>();
//        int[][] graph = new int[n+1][];

        //邻接表
        for (int[] time : times) {
            map.putIfAbsent(time[0],new ArrayList<>());
            map.get(time[0]).add(new int[]{time[1],time[2]});
        }
        //Dijkstra算法
        int[] dist = dijkstra(n, k, map);
        //4.计算最优解
        int ans = 0;
        for (int i = 1; i <= n; i++) {
            ans = Math.max(ans, dist[i]);
        }
        return ans == 222 ? -1 : ans;
    }

    private int[] dijkstra(int n, int k, Map<Integer, List<int[]>> graph){
        int[] dist = new int[n+1];
        boolean[] vis = new boolean[n+1];
        Arrays.fill(dist,222);
        Arrays.fill(vis, false);
        dist[k] = 0;
        //小根堆
        PriorityQueue<int[]> q = new PriorityQueue<>((a, b)->a[1]-b[1]);//id，weight
        q.add(new int[]{k, 0});
        while (!q.isEmpty()) {
            //1.找出最便宜节点
            int[] poll = q.poll();
            int id = poll[0], weight = poll[1];
            //3.重复，直到遍历每一个节点
            if (vis[id]) continue;
            vis[id] = true;
            //2.更新邻居开销
            List<int[]> neighbour = graph.get(id);
            if (null == neighbour){
                continue;
            }
            for (int[] ne : neighbour) {
                int tid = ne[0], tweight = ne[1];
                int tDist = dist[id] + tweight;
                if (dist[tid] > tDist){
                    dist[tid] = tDist;
                    q.add(new int[]{tid, tDist});
                }
            }
        }
        return dist;
    }
}
