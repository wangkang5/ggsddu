package com.leetcode;

import java.util.*;

import static java.util.stream.Collectors.toList;

/**
 * @ClassName N802
 * @Author 19012511
 * @Date 2021/8/5 15:57
 **/
public class N802 {

    public List<Integer> eventualSafeNodes(int[][] graph) {
        int n = graph.length;
        int[] color = new int[n];
        List<Integer> ans = new ArrayList<Integer>();
        for (int i = 0; i < n; ++i) {
            if (safe(graph, color, i)) {
                ans.add(i);
            }
        }
        return ans;
    }

    public boolean safe(int[][] graph, int[] color, int x) {
        if (color[x] > 0) {
            return color[x] == 2;
        }
        color[x] = 1;
        for (int y : graph[x]) {
            if (!safe(graph, color, y)) {
                return false;
            }
        }
        color[x] = 2;
        return true;
    }

    public static void main(String[] args) {
        int[][] graph = new int[][]{{1,2},{2,0},{3},{}};
        N802 n802 = new N802();
        System.out.println(n802.eventualSafeNodes(graph));
    }


}
