package com.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName N863
 * @Author 19012511
 * @Date 2021/7/28 9:21
 **/
public class N863 {
    Map<TreeNode,TreeNode> parentMap = new HashMap<>();
    List<Integer> res = new ArrayList<>();
    public List<Integer> distanceK(TreeNode root, TreeNode target, int k) {
        findParent(root);
        dfs(target,null,0,k);
        return res;
    }

    private void findParent(TreeNode root){
        if (null != root.left){
            parentMap.put(root.left,root);
            findParent(root.left);
        }
        if (null != root.right){
            parentMap.put(root.right,root);
            findParent(root.right);
        }
    }

    private void dfs(TreeNode node,TreeNode origin, int cur, int k) {
        if (null == node) {
            return;
        }
        System.out.println(node.val);
        if (cur == k){
            res.add(node.val);
            return;
        }
        if (node.left != origin){
            dfs(node.left,node,cur+1,k);
        }
        if (node.right != origin){
            dfs(node.right,node,cur+1,k);
        }
        if (parentMap.get(node) != origin){
            System.out.println(parentMap.get(node).val);
            dfs(parentMap.get(node),node,cur+1,k);
        }
    }
}
