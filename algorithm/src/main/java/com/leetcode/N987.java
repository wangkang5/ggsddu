package com.leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;


public class N987 {
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    PriorityQueue<int[]> p = new PriorityQueue<>((a, b) -> {// col, row, val
        if (a[0] != b[0]) return a[0] - b[0];
        if (a[1] != b[1]) return a[1] - b[1];
        return a[2] - b[2];
    });

    public List<List<Integer>> verticalTraversal(TreeNode root) {
        int[] fa = new int[]{0,0,root.val};
        p.add(fa);
        dfs(root,fa);
        List<List<Integer>> res = new ArrayList<>();
        while (!p.isEmpty()){
            List<Integer> tmp = new ArrayList<>();
            int[] poll = p.peek();
            while (!p.isEmpty() && poll[0] == p.peek()[0]){
                tmp.add(p.poll()[2]);
            }
            res.add(tmp);
        }

        return res;
    }

    private void dfs(TreeNode root, int[] fa){
        if (null != root.left){
            int[] lnode = new int[]{fa[0]-1,fa[1]+1,root.left.val};
            p.add(lnode);
            dfs(root.left,lnode);
        }
        if (null != root.right){
            int[] rnode = new int[]{fa[0]+1,fa[1]+1,root.right.val};
            p.add(rnode);
            dfs(root.right,rnode);
        }
    }
}
