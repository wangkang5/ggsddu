package com.leetcode;

public class O42 {
    public int maxSubArray(int[] nums) {
        int len = nums.length;
        int res = 0;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i< len; i++){
            if (res > 0){
                res += nums[i];
            } else {
                res = nums[i];
            }
            System.out.println(res);
            max = Math.max(max,res);
        }
        return max;
    }

    public static void main(String[] args) {
        O42 o42 = new O42();
        o42.maxSubArray(new int[]{-2,1,-3,4,-1,2,1,-5,4});
    }
}
