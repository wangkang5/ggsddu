package com.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName O52
 * @Author 19012511
 * @Date 2021/7/21 9:07
 **/
public class O52 {
    public class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
            next = null;
        }
    }

    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        Set set = new HashSet();
        while (null != headA){
            set.add(headA);
            headA = headA.next;
        }

        while (null != headB){
            if (set.contains(headB)){
                return headB;
            }
            headB = headB.next;
        }
        return null;
    }

    //暴力
    public ListNode getIntersectionNode1(ListNode headA, ListNode headB) {
        while (null != headA){
            ListNode headBB = headB;
            while (null != headBB){
                if (headA == headBB){
                    return headBB;
                }
                headBB = headBB.next;
            }
            headA = headA.next;
        }
        return null;
    }
}



