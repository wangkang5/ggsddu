package com.leetcode;

/**
 * @ClassName O53
 * @Author 19012511
 * @Date 2021/7/16 10:44
 **/
public class O53 {
    public int search(int[] nums, int target) {

        int res = 0;
        int l = 0, r = nums.length-1;
        int mid = 0;
        while (l<r){
            mid = (l+r)/2;
            if (nums[mid] >= target){
                r = mid;
            } else {
                l = mid+1;
            }
        }
        while (l<nums.length){
            if (nums[l] == target){
                res++;
                l++;
            } else {
                break;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        O53 o53 = new O53();
        System.out.println(o53.search(new int[]{2,2},2));
    }
}
