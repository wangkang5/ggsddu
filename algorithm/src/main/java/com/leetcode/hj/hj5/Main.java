package com.leetcode.hj.hj5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String s = scanner.nextLine();
            System.out.println(Integer.valueOf(s.substring(2), 16));

        }
    }

    public static void main52(String[] args) {
        //方法3
        // 十进制数字：16, 十六进制表示：1a
        Scanner sc = new Scanner(System.in);
        // 十进制转换十六进制字符串;
        String numHex = Integer.toHexString(sc.nextInt());
        System.out.println(numHex);
        // 十六进制字符串转换为十进制数字
        //(Integer.valueOf 或 Integer.parseInt均可指定radix)
        System.out.println(Integer.valueOf(sc.nextLine(), 16));
    }

    public static void main51(String[] args) {
        //方法2
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            System.out.println(Integer.parseInt(sc.next().replaceAll("x", ""), 16));
        }
    }


    public static void main5(String[] args) {
        // 16进制数字转换成10进制
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            //下面这两行代码是为了把16进制的数最开始会有0x的标识  将他俩去掉
            String s = sc.nextLine();
            s = s.substring(2);
            System.out.println(Long.parseLong(s, 2));
        }
    }
}