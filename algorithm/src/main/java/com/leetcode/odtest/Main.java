package com.leetcode.odtest;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String[][] keyboard = new String[10][];
        keyboard[0] = new String[]{" "};
        keyboard[1] = new String[]{",", "."};
        keyboard[2] = new String[]{"a", "b", "c"};
        keyboard[3] = new String[]{"d", "e", "f"};
        keyboard[4] = new String[]{"g", "h", "i"};
        keyboard[5] = new String[]{"j", "k", "l"};
        keyboard[6] = new String[]{"m", "n", "o"};
        keyboard[7] = new String[]{"p", "q", "r", "s"};
        keyboard[8] = new String[]{"t", "u", "v"};
        keyboard[9] = new String[]{"w", "x", "y", "z"};
        boolean isNum = true;
        String ans = "";

        int i = 0;
        int n = str.length();
        Character lastNum = null;
        int count = 0;
        while (i < n) {
            char c = str.charAt(i);
            if (isNum && Character.isDigit(c)) {
                ans += c;
                i++;
            } else if (isNum && '#' == c) {
                isNum = isNum == true ? false : true;
                i++;
                continue;
            } else if (isNum && '/' == c) {
                i++;
                continue;
            } else {
                //英文模式
                if (null == lastNum) {
                    if ('#' == c) {
                        isNum = isNum == true ? false : true;
                        i++;
                        continue;
                    } else if ('/' == c) {
                        i++;
                        continue;
                    } else {
                        //等于Character.isDigit(c)
                        lastNum = c;
                        count++;
                        i++;
                        continue;
                    }
                } else {
                    if (lastNum.equals(c)) {
                        count++;
                        i++;
                        continue;
                    } else {
                        //英文模式确定输入
                        int num = lastNum-'0';
                        int x = count % (keyboard[num].length);
                        if (x == 0){
                            x = keyboard[num].length;
                        }
                        ans+=keyboard[num][x-1];
                        if ('#' == c) {
                            isNum = isNum == true ? false : true;
                            lastNum = null;
                            count = 0;
                        } else if ('/' == c) {
                            lastNum = null;
                            count = 0;
                        } else {
                            lastNum = c;
                            count = 1;
                        }
                        i++;
                        continue;
                    }
                }
            }
        }
        if (!isNum && null != lastNum){
            int num = lastNum-'0';
            int x = count % (keyboard[num].length);
            if (x == 0){
                x = keyboard[num].length;
            }
            ans+=keyboard[num][x-1];
        }
        System.out.println(ans);
    }
}
