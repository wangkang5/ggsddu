package com.leetcode.oj.oj1;


import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        LinkedHashMap<String, Integer> map = new LinkedHashMap<>();
        while (scanner.hasNextLine()){
            String s = scanner.nextLine();
            String[] s1 = s.split(" ");
            String[] split = s1[0].split("\\\\");
            String fileName = split[split.length-1];
            if (fileName.length()>16){
                fileName = fileName.substring(fileName.length()-16);
            }
            fileName = fileName + " " + s1[1];
            map.put(fileName,map.getOrDefault(fileName,0)+1);
        }
        int count=0;
        for(String string:map.keySet()){
            count++;
            if(count>(map.keySet().size()-8)) //输出最后八个记录
                System.out.println(string+" "+map.get(string));
        }

    }

}