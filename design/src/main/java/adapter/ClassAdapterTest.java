package adapter;

/**
 * 类适配器
 * @ClassName ClassAdapterTest
 * @Author 19012511
 * @Date 2021/5/17 20:13
 **/
public class ClassAdapterTest {
    public static void main(String[] args) {
        System.out.println("类适配器模式测试：");
        //利用适配器创建一个交流电
        Target target = new ClassAdapter();
        //发电
        target.request();
    }
}

//目标接口 --被调用者 交流电
interface Target
{
    //发送220V交流电
    public void request();
}
//适配者接口 --调用者 笔记本电脑
class Adaptee
{
    //接收12V直流电
    public void specificRequest()
    {
        System.out.println("适配者中的业务代码被调用！");
    }
}
//类适配器类 --适配器
class ClassAdapter extends Adaptee implements Target
{
    //转换电能
    public void request()
    {
        specificRequest();
    }
}