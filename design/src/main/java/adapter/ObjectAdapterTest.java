package adapter;

/**
 * 对象适配器
 * @ClassName ObjectAdapterTest
 * @Author 19012511
 * @Date 2021/5/17 20:28
 **/
public class ObjectAdapterTest {
    public static void main(String[] args)
    {
        System.out.println("对象适配器模式测试：");
        Adaptee adaptee = new Adaptee();
        //交流电 = 适配器包装（笔记本电脑）
        Target target = new ObjectAdapter(adaptee);
        target.request();
    }
}


/**
 * 对象适配器类
 * ObjectAdapter适配器电源
 * Target交流电
 */
class ObjectAdapter implements Target
{
    //笔记本电脑
    private Adaptee adaptee;
    public ObjectAdapter(Adaptee adaptee)
    {
        this.adaptee=adaptee;
    }
    public void request()
    {
        adaptee.specificRequest();
    }
}