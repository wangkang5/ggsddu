package composite;

import java.util.ArrayList;

/**
 * 安全组合模式
 * @ClassName CompositePattern
 * @Author 19012511
 * @Date 2021/5/17 11:11
 **/
public class CompositePattern2 {
    public static void main(String[] args) {
        Composite2 c0 = new Composite2();
        Composite2 c1 = new Composite2();
        Component2 leaf1 = new Leaf2("1");
        Component2 leaf2 = new Leaf2("2");
        Component2 leaf3 = new Leaf2("3");
        c0.add(leaf1);
        c0.add(c1);
        c1.add(leaf2);
        c1.add(leaf3);
        c0.operation();
    }
}

//抽象构件
interface Component2 {
    public void operation();
}

//树叶构件
class Leaf2 implements Component2 {
    private String name;
    public Leaf2(String name) {
        this.name = name;
    }
    public void add(Component2 c) {
    }
    public void remove(Component2 c) {
    }
    public Component2 getChild(int i) {
        return null;
    }
    public void operation() {
        System.out.println("安全树叶" + name + "：被访问！");
    }
}

//树枝构件
class Composite2 implements Component2 {
    private ArrayList<Component2> children = new ArrayList<Component2>();
    public void add(Component2 c) {
        children.add(c);
    }
    public void remove(Component2 c) {
        children.remove(c);
    }
    public Component2 getChild(int i) {
        return children.get(i);
    }
    public void operation() {
        for (Object obj : children) {
            ((Component2) obj).operation();
        }
    }
}
