package factory;

/**
 * 抽象工厂模式
 * 抽象工厂类 多个newProduct()方法，创建不同等级产品
 * 具体工厂类
 * 多个抽象产品类
 * 具体产品类
 * @ClassName AbstractFactoryTest
 * @Author 19012511
 * @Date 2021/5/19 20:03
 **/
public class AbstractFactoryTest {
    public static void main(String[] args) {
        AbstractFactory farm;
        Product1 animal;
        Product2 plant;
        farm = new ConcreteFactory1();
        animal = farm.newProduct1();
        plant = farm.newProduct2();
        animal.show();
        plant.show();
    }
}
interface AbstractFactory {
    public Product1 newProduct1();
    public Product2 newProduct2();
}
interface Product1{
    void show();
}
interface Product2{
    void show();
}
class ConcreteFactory1 implements AbstractFactory {
    public Product1 newProduct1() {
        System.out.println("具体工厂 1 生成-->具体产品 11...");
        return new ConcreteProduct11();
    }
    public Product2 newProduct2() {
        System.out.println("具体工厂 1 生成-->具体产品 21...");
        return new ConcreteProduct21();
    }
}
class ConcreteFactory2 implements AbstractFactory {
    public Product1 newProduct1() {
        System.out.println("具体工厂 2 生成-->具体产品 11...");
        return new ConcreteProduct12();
    }
    public Product2 newProduct2() {
        System.out.println("具体工厂 2 生成-->具体产品 21...");
        return new ConcreteProduct22();
    }
}

class ConcreteProduct11 implements Product1{
    public void show() {
        System.out.println("show ConcreteProduct11");
    }
}
class ConcreteProduct12 implements Product1{
    public void show() {
        System.out.println("show ConcreteProduct12");
    }
}
class ConcreteProduct21 implements Product2{
    public void show() {
        System.out.println("show ConcreteProduct21");
    }
}
class ConcreteProduct22 implements Product2{
    public void show() {
        System.out.println("show ConcreteProduct22");
    }
}