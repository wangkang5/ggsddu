package factory;

/**
 * 工厂模式
 * 抽象产品类
 * 具体产品类
 * 抽象工厂类
 * 具体工厂类
 * @ClassName FactoryTest
 * @Author 19012511
 * @Date 2021/5/19 19:54
 **/
public class FactoryTest {
    public static void main(String[] args) {
        Factory factory = new FactoryA();
        Product product = factory.newProduct();
        product.show();
    }
}

interface Product{
    void show();
}

class ProductA implements Product{
    public void show() {
        System.out.println("具体产品A显示...");
    }
}
class ProductB implements Product{
    public void show() {
        System.out.println("具体产品B显示...");
    }
}
interface Factory {
    Product newProduct();
}
class FactoryA implements Factory {
    public Product newProduct() {
        System.out.println("具体工厂A生成-->具体产品A...");
        return new ProductA();
    }
}
class FactoryB implements Factory {
    public Product newProduct() {
        System.out.println("具体工厂B生成-->具体产品B...");
        return new ProductB();
    }
}