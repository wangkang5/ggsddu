package memento;

/**
 * @ClassName MementoPattern
 * @Author 19012511
 * @Date 2021/5/19 16:01
 **/
public class MementoPattern {
    public static void main(String[] args) {
        Originator or = new Originator();//发起人
        Caretaker cr = new Caretaker();//管理者
        or.setState("S0");
        System.out.println("初始状态:" + or.getState());
        cr.setMemento(or.createMemento()); //保存状态
        or.setState("S1");
        System.out.println("新的状态:" + or.getState());
        or.restoreMemento(cr.getMemento()); //恢复状态
        System.out.println("恢复状态:" + or.getState());
    }
}

//备忘录
class Memento {
    private String state;
    public Memento(String state) {
        this.state = state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getState() {
        return state;
    }
}
//发起人
class Originator {
    private String state;
    public void setState(String state) {
        this.state = state;
    }
    public String getState() {
        return state;
    }
    public Memento createMemento() {
        return new Memento(state);
    }
    //提供了一种可以恢复状态的机制。当用户需要时能够比较方便地将数据恢复到某个历史的状态。
    public void restoreMemento(Memento m) {
        this.setState(m.getState());
    }
    //发起人不需要管理和保存其内部状态的各个备份，所有状态信息都保存在备忘录中，并由管理者进行管理，这符合单一职责原则。
}
//管理者
class Caretaker {
    //实现了内部状态的封装。除了创建它的发起人之外，其他对象都不能够访问这些状态信息。
    private Memento memento;
    public void setMemento(Memento m) {
        memento = m;
    }
    public Memento getMemento() {
        return memento;
    }
}