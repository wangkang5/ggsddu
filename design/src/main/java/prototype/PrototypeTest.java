package prototype;

/**
 * @ClassName PrototypeTest
 * @Author 19012511
 * @Date 2021/5/25 19:59
 **/
public class PrototypeTest {
    public static void main(String[] args) throws CloneNotSupportedException {
//        Realizetype obj1 = new Realizetype();
//        Realizetype obj2 = (Realizetype) obj1.clone();
//        System.out.println("obj1==obj2?" + (obj1 == obj2));

        PrototypeTest prototypeTest = new PrototypeTest();
        Object clone = prototypeTest.clone();
        System.out.println(prototypeTest);
        System.out.println(clone);

    }
}

//具体原型类
class Realizetype implements Cloneable {
    Realizetype() {
        System.out.println("具体原型创建成功！");
    }
    @Override
    public Object clone() throws CloneNotSupportedException {
        System.out.println("具体原型复制成功！");
        return (Realizetype) super.clone();
    }
}