package proxy.dynamicproxy;

public interface IPerson {
    void findTeacher(); //找老师

    void findParent(); //找家长
}
