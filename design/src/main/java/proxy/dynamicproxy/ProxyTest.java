package proxy.dynamicproxy;

public class ProxyTest {

    public static void main(String[] args) {
        JdkFuDao jdkFuDao = new JdkFuDao();
        IPerson instance = jdkFuDao.getInstance(new ZhangSan());
        instance.findTeacher();
    }
}
