package proxy.dynamicproxy;

public class ZhangSan implements IPerson {

    @Override
    public void findTeacher() {
        System.out.println("学生张三提出要求");
    }

    @Override
    public void findParent() {
        System.out.println("儿子张三提出要求");
    }
}
