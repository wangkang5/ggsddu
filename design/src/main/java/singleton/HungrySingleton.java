package singleton;

/**
 * @ClassName HungrySingleton
 * @Author 19012511
 * @Date 2021/5/19 17:53
 **/
public class HungrySingleton {

    private static final HungrySingleton instance = new HungrySingleton();

    private HungrySingleton() {
        //private 避免类在外部被实例化
    }

    public static HungrySingleton getInstance (){
        return instance;
    }
}
