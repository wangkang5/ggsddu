package singleton;

/**
 * @ClassName LazySingleton
 * @Author 19012511
 * @Date 2021/5/19 17:40
 **/
public class LazySingleton {

    private static volatile LazySingleton instance = null; //保证 instance 在所有线程中同步

    private LazySingleton() {
        //private 避免类在外部被实例化
    }

    //getInstance 方法前加同步
    public static synchronized LazySingleton getInstance() {
        if (null == instance) {
            instance = new LazySingleton();
        }
        return instance;
    }

    //双重检查锁模式的写法 double-check
    public static LazySingleton getInstance2(){
        //第一个check，如果没有的话，所有线程都会串行执行，效率低下
        if (null == instance){
            synchronized (LazySingleton.class){
                //第二个check，如果两个线程都可以通过第一重的 if 判断，其二等待锁释放后还需要判断下，否则会new第二个实例
                if (null == instance){
                    instance = new LazySingleton();
                }
            }
        }
        return instance;
    }
}
