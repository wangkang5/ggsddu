package mapper;

import model.Demo;
import model.PageBO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ClassName DemoMapper
 * @Author 19012511
 * @Date 2021/6/2 11:51
 **/
public interface DemoMapper {
    public Demo getById(@Param("id") long id, @Param("page")PageBO page);
    public List<Demo> getAll(@Param("page")PageBO page);
}
