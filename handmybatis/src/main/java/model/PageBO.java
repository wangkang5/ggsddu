package model;

/**
 * @ClassName PageBO
 * @Author 19012511
 * @Date 2021/6/21 20:13
 **/
public class PageBO {
    private int page;
    private int limit;
    private int count;
    private int start;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getStart() {
        if ( 1 == page){
            return 0;
        } else {
            return page*limit -1;
        }
    }

    public void setStart(int start) {
        this.start = start;
    }

    public PageBO() {
    }

    public PageBO(int page, int limit) {
        this.page = page;
        this.limit = limit;
    }
}
