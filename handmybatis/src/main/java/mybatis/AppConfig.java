package mybatis;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName AppConfig
 * @Author 19012511
 * @Date 2021/6/2 15:08
 **/
@Configuration
@LubanScan
//@ComponentScan({"mybatis","mapper"})
public class AppConfig {
}
