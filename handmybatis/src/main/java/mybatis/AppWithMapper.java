package mybatis;

import mapper.DemoMapper;
import model.Demo;
import model.PageBO;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 优雅的使用方式 mapper
 * @ClassName App
 * @Author 19012511
 * @Date 2021/6/2 11:40
 **/
public class AppWithMapper {
    public static void main(String[] args) throws IOException {
        String resource = "mybatis-config.xml";

        InputStream inputStream = Resources.getResourceAsStream(resource);

        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //使用sqlSession直接查询
        SqlSession sqlSession = sqlSessionFactory.openSession(true); //true 不开启事务，自动提交
        //使用Mapper
        DemoMapper demoMapper = sqlSession.getMapper(DemoMapper.class);
        Demo demo = demoMapper.getById(1L,new PageBO(1,1));
        System.out.println(demo);
        List<Demo> demos = demoMapper.getAll(new PageBO(1,1));
        System.out.println(demos);

    }
}
