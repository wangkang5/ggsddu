package mybatis;

import model.Demo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 不强制需要Mapper.java类，通过二维坐标就可以进行查询操作
 * @ClassName App
 * @Author 19012511
 * @Date 2021/6/2 11:40
 **/
public class AppWithNameSpace {
    public static void main(String[] args) throws IOException {
        String resource = "mybatis-config.xml";

        InputStream inputStream = Resources.getResourceAsStream(resource);

        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //使用sqlSession直接查询
        SqlSession sqlSession = sqlSessionFactory.openSession(true); //true 不开启事务，自动提交
        Demo demo = sqlSession.selectOne("mapper.DemoMapper.getById",1L);
        System.out.println(demo);

        List<Demo> demos = sqlSession.selectList("mapper.DemoMapper.getAll");
        System.out.println(demos);

    }
}
