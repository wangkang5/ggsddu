package mybatis;

import mapper.DemoMapper;
import model.Demo;
import model.PageBO;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 不强制需要Mapper.java类，通过二维坐标就可以进行查询操作
 * 来源：https://www.cnblogs.com/raitorei/articles/12880617.html
 * @ClassName App
 * @Author 19012511
 * @Date 2021/6/2 11:40
 **/
public class AppWithSpring {

    public static void main(String[] args) throws IOException {
        Logger logger = Logger.getLogger(AppWithSpring.class);

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        System.out.println("lubanFactoryBean: " + context.getBean("lubanDemoMapper"));
        System.out.println("lubanFactoryBean-class: " + context.getBean("lubanDemoMapper").getClass());

        DemoMapper demoMapper = (DemoMapper)context.getBean("lubanDemoMapper");

        Demo demo = demoMapper.getById(1L,new PageBO(1,1));
        System.out.println(demo);
        List<Demo> demos = demoMapper.getAll(new PageBO(3,1));
        System.out.println(demos);

    }
}
