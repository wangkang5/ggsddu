package mybatis;

import mapper.DemoMapper;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.core.type.filter.TypeFilter;

import java.util.Map;

/**
 * @ClassName LuBanLuban
 * @Author 19012511
 * @Date 2021/6/2 15:37
 **/
public class LuBanImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition();
        //扫描注解
//        Map<String, Object> annotationAttributes = importingClassMetadata
//                .getAnnotationAttributes(ComponentScan.class.getName());
//        String[] basePackages = (String[]) annotationAttributes.get("basePackages");
//        for (String basePackage : basePackages) {
//            //扫描类
//            ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(registry, false);
//            scanner.scan(basePackages);
//        }

        AbstractBeanDefinition beanDefinition = builder.getBeanDefinition();
        beanDefinition.setBeanClassName(LubanFactoryBean.class.getName());
        // 当前BeanDefinition在生成bean对象时，会通过调用LubanFactoryBean的构造方法来生成，并传入DemoMapper
        beanDefinition.getConstructorArgumentValues().addGenericArgumentValue(DemoMapper.class.getName());
        // 添加beanDefinition
        registry.registerBeanDefinition("luban"+DemoMapper.class.getSimpleName(), beanDefinition);

    }
}
