package mybatis;

import mapper.DemoMapper;
import model.Demo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;

/**
 * @ClassName LubanFactoryBean
 * @Author 19012511
 * @Date 2021/6/2 15:10
 **/
@Component
public class LubanFactoryBean implements FactoryBean {
    private Class mapperInterface;
    public LubanFactoryBean(Class mapperInterface) {
        this.mapperInterface = mapperInterface;
    }
    @Override
    public Object getObject() throws Exception {
        Object proxyInstance = Proxy.newProxyInstance(LubanFactoryBean.class.getClassLoader(), new Class[]{mapperInterface}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if (Object.class.equals(method.getDeclaringClass())) {
                    return method.invoke(this, args);
                } else {
                    Object mapper = before();
                    Object invoke = method.invoke(mapper,args);
                    after();
                    return invoke;
                }
            }
        });

        return proxyInstance;
    }

    @Override
    public Class<?> getObjectType() {
        return mapperInterface;
    }

    public Object before() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        //使用sqlSession直接查询
        SqlSession sqlSession = sqlSessionFactory.openSession(true); //true 不开启事务，自动提交
        //使用Mapper
        Object mapper = sqlSession.getMapper(mapperInterface);
        return mapper;
    }

    public void after(){}

    public static void main(String[] args) {
        new LubanFactoryBean(DemoMapper.class);
    }
}
