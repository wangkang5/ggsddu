package mybatis;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @ClassName LubanScan
 * @Author 19012511
 * @Date 2021/6/2 15:26
 * @Import支持 三种方式
 * 1.带有@Configuration的配置类(4.2 版本之前只可以导入配置类，4.2版本之后 也可以导入 普通类)
 * 2.ImportSelector 的实现
 * 3.ImportBeanDefinitionRegistrar 的实现
 **/
@Retention(RetentionPolicy.RUNTIME)
@Import(LuBanImportBeanDefinitionRegistrar.class)
public @interface LubanScan {

}
