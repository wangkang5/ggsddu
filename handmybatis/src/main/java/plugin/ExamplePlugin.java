package plugin;

import model.PageBO;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.DefaultReflectorFactory;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Properties;

/**
 * @ClassName ExamplePlugin
 * @Author 19012511
 * @Date 2021/6/17 19:41
 **/
@Intercepts({@Signature(type = StatementHandler.class, method ="prepare", args = {Connection.class, Integer.class})})
public class ExamplePlugin implements Interceptor {

    private final static Logger logger = Logger.getLogger(ExamplePlugin.class);

    private String end="";

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        StatementHandler statementHandler = (StatementHandler) invocation.getTarget();

        MetaObject metaObject = MetaObject.forObject(statementHandler, SystemMetaObject.DEFAULT_OBJECT_FACTORY, SystemMetaObject.DEFAULT_OBJECT_WRAPPER_FACTORY, new DefaultReflectorFactory());
        String value = (String) metaObject.getValue("delegate.mappedStatement.id");
        if (true){
            //执行count sql
            Connection connection = (Connection) invocation.getArgs()[0];
            String sql = statementHandler.getBoundSql().getSql();
            logger.info("原始sql："+sql);
            String count = "select count(0) from (" + sql + ") a";
            PreparedStatement preparedStatement = connection.prepareStatement(count);
            ParameterHandler parameterHandler = (ParameterHandler) metaObject.getValue("delegate.parameterHandler");
            parameterHandler.setParameters(preparedStatement);
            ResultSet resultSet = preparedStatement.executeQuery();

            //获取pageBO
            HashMap<String,Object> map = (HashMap<String,Object>) parameterHandler.getParameterObject();
            PageBO parameterObject = (PageBO) map.get("page");
            if (resultSet.next()){
                parameterObject.setCount(resultSet.getInt(1));
            }
            logger.info("countsql:"+count+":"+parameterObject.getCount());
            resultSet.close();
            preparedStatement.close();
            //修改sql，添加limit
            String pageSql = sql + " limit " + parameterObject.getStart() + " , " + parameterObject.getLimit();
            logger.info("pageSql:"+pageSql);

            metaObject.setValue("delegate.boundSql.sql",pageSql);
        }
        return invocation.proceed();
    }

    @Override
    public Object plugin(Object targe) {
        return Plugin.wrap(targe,this);
    }

    @Override
    public void setProperties(Properties properties) {
        end = properties.getProperty("end");
    }
}
