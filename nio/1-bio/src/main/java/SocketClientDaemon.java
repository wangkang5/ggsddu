import java.util.concurrent.CountDownLatch;

/**
 * @ClassName SocketClientDaemon
 * @Author 19012511
 * @Date 2021/6/24 16:42
 **/
public class SocketClientDaemon {
    public static void main(String[] args) throws Exception{
        Integer clientNumber = 20;
        CountDownLatch countDownLatch = new CountDownLatch(clientNumber);

        for (int index = 0; index < clientNumber ; index++, countDownLatch.countDown()){
            SocketClientRequestThread client = new SocketClientRequestThread(countDownLatch, index);
            new Thread(client).start();
        }

        synchronized (SocketClientDaemon.class){
            SocketClientDaemon.class.wait();
        }
    }
}
