import cn.hutool.json.JSON;
import cn.hutool.json.JSONString;
import cn.hutool.json.JSONUtil;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.Socket;
import java.text.NumberFormat;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

/**
 * @ClassName SocketClientRequestThread
 * @Author 19012511
 * @Date 2021/6/24 16:45
 **/
public class SocketClientRequestThread implements Runnable {

    static {
        BasicConfigurator.configure();
    }
    private static final Logger logger = Logger.getLogger(SocketClientRequestThread.class);
    private CountDownLatch countDownLatch;
    //线程编号
    private Integer clientIndex;

    public SocketClientRequestThread(CountDownLatch countDownLatch, Integer clientIndex) {
        this.countDownLatch = countDownLatch;
        this.clientIndex = clientIndex;
    }

    @Override
    public void run() {
        Socket socket = null;
        OutputStream clientRequest = null;
        InputStream clientResponse = null;

        try {
            socket = new Socket("127.0.0.1", 83);
            clientRequest = socket.getOutputStream();
            clientResponse = socket.getInputStream();
            //等待所有线程启动，然后一起发送请求
            countDownLatch.await();
            //发送请求
            clientRequest.write(("this is " + clientIndex + " client request").getBytes());
            clientRequest.flush();
            //等待服务器返回信息
            logger.info("No. " + this.clientIndex + " client send message finish, please wait response !!!");

            int maxLen = 1024;
            byte[] contextBytes = new byte[maxLen];
            int realLen;
            String message = "";
            //等待 & 读取服务器返回信息
            while ((realLen = clientResponse.read(contextBytes,0,maxLen)) != -1){
                message += new String(contextBytes, 0, realLen);
            }
            logger.info("No. " + this.clientIndex + " client receive message: "+ message);
        } catch (IOException|InterruptedException e) {
            logger.error(e.getMessage(),e);
        } finally {
            try {
                if(clientRequest != null) {
                    clientRequest.close();
                }
                if(clientResponse != null) {
                    clientResponse.close();
                }
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

}
