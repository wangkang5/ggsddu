import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class TestSelector {
    public static Selector selector;

    static {
        try {
            selector = Selector.open();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString(1073741824));
        System.out.println(Integer.toBinaryString(1073741824<<1));
        System.out.println(1073741824<<1);
    }
    public static void main1(String[] args) throws IOException {
        selector = Selector.open();
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(new InetSocketAddress(8080));
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        while (!Thread.interrupted()) {
            selector.select();
            Set selected = selector.selectedKeys();
            Iterator<SelectionKey> it = selected.iterator();
            while (it.hasNext()) {
                SelectionKey key = it.next();
                it.remove();
                dispatch(key);
            }
        }
    }

    private static void dispatch(SelectionKey key) throws IOException {
        if (key.isAcceptable()) {
            register(key);
        } else if (key.isReadable()) {
            read(key);
        } else if (key.isWritable()) {
            write(key);
        }
    }

    private static void register(SelectionKey key) throws IOException {
        System.out.println(key.isAcceptable());
        System.out.println(key.isReadable());
        ServerSocketChannel server = (ServerSocketChannel) key.channel();
        SocketChannel channel = server.accept();
        channel.write(ByteBuffer.wrap("send a message to client."
                .getBytes()));
        channel.configureBlocking(false);
        channel.register(selector, SelectionKey.OP_READ);
    }



    private static void read(SelectionKey key) throws IOException {
        SocketChannel clientChannel = (SocketChannel) key.channel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        int length = clientChannel.read(buffer);
        String request = new String(buffer.array(),0,length);
        System.out.println("-----server receive client's request:" + request);
        clientChannel.register(selector, SelectionKey.OP_WRITE);

    }

    private static void write(SelectionKey key) throws IOException {
        SocketChannel clientChannel = (SocketChannel) key.channel();
        clientChannel.write(ByteBuffer.wrap("hello client".getBytes()));

    }
}
