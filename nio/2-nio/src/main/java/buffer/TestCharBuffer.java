package buffer;

import java.nio.CharBuffer;

public class TestCharBuffer {
    public static void main(String[] args) {
        String ss = "hello world";
        CharBuffer charBuffer = CharBuffer.allocate(9);
//        System.out.println("init get:"+charBuffer.get());
        fillBuffer(charBuffer,ss);
        charBuffer.flip();
        charBuffer.rewind();
        System.out.println("before:"+charBuffer.get());
        System.out.println("after:"+charBuffer.get());
        System.out.println("after:"+charBuffer.get());
        System.out.println("after:"+charBuffer.get());
        System.out.println("after:"+charBuffer.get());
        System.out.println("after:"+charBuffer.get());
        System.out.println("after:"+charBuffer.get());
        System.out.println("after:"+charBuffer.get());
        System.out.println("after:"+charBuffer.get());
        System.out.println("after:"+charBuffer.get());
        System.out.println("after:"+charBuffer.get());
        System.out.println("after:"+charBuffer.get());

        System.out.println("after:"+charBuffer.get());
    }

    private static void fillBuffer(CharBuffer buffer,String ss){
        for (int i=0; i< ss.length(); i++){
            System.out.println(ss.charAt(i));
            buffer.put(ss.charAt(i));
        }
    }
}
