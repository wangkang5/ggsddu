import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.math.MathUtil;
import cn.hutool.core.util.NumberUtil;
import com.sun.javaws.progress.Progress;
import pojo.WordInfo;
import service.WordUtil;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.metal.MetalLookAndFeel;
import java.awt.*;
import java.awt.event.*;
import java.awt.font.TextAttribute;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.sun.deploy.uitoolkit.ToolkitStore.dispose;

/**
 * @ClassName HelloSwing
 * @Author 19012511
 * @Date 2021/5/27 16:36
 **/
public class HelloSwing {
    public static int layWidth = 300;
    public static int layHeight = 150;
    public static Integer start = null;
    public static Integer end = null;
    public static Integer intervalSecond = 30;
    public static long tempTimestamp = 0L;
    public static int currentProgress = 20;


    //记录鼠标位置
    public static Point origin = new Point();
    //开启轮播
    public static boolean loop = true;
    //图片资源
    public static ImageIcon play;
    public static ImageIcon pause;
    //进度条对象
    public static JProgressBar jProgressBar;

    static {
        try {
            play = new ImageIcon(ImageIO.read(ResourceUtil.getStream("play.png")));
            pause = new ImageIcon(ImageIO.read(ResourceUtil.getStream("pause.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        jProgressBar = createJProgressBar();
    }

    private static java.util.List<WordInfo> wordInfoList = WordUtil.getWordInfoList(start,end);

    public static void updateWordInfoList(Integer start, Integer end){
        wordInfoList = WordUtil.getWordInfoList(start,end);
    }

    /**{
     * 创建并显示GUI。出于线程安全的考虑，
     * 这个方法在事件调用线程中调用。
     */
    private static JLabel createAndShowGUI() throws UnsupportedLookAndFeelException {
        // 确保一个漂亮的外观风格
        UIManager.setLookAndFeel(new MetalLookAndFeel());
        // 创建及设置窗口
        JFrame frame = createJFrame();
        JPanel jPanel = new JPanel(createLayout());
        jPanel.setBounds(0,0,layWidth,layHeight);//添加到 JLayeredPane 内的组件需要明确指定组在位置和宽高，否则不显示（类似绝对布局）
//        frame.setContentPane(jPanel);
//        frame.setLayout(createLayout());
        JLabel northLabel = new JLabel(); northLabel.setPreferredSize(new Dimension(layWidth,10));
        JLabel westLable = new JLabel(); westLable.setPreferredSize(new Dimension(20,layHeight));
        JLabel eastLabel = new JLabel(); eastLabel.setPreferredSize(new Dimension(20,layHeight));
        jPanel.add(northLabel,BorderLayout.NORTH);
        jPanel.add(jProgressBar,BorderLayout.SOUTH);
        jPanel.add(westLable,BorderLayout.WEST);
        jPanel.add(eastLabel,BorderLayout.EAST);

        JLabel jLabel = createJLabel();
        jPanel.add(jLabel,BorderLayout.CENTER);

        JButton jButton = createJButton();

        JLayeredPane layeredPane = new JLayeredPane();
        layeredPane.add(jPanel, new Integer(300));
        layeredPane.add(jButton, new Integer(400));
        frame.setContentPane(layeredPane);
        //frame.getContentPane().add(label);
        //frame.pack(); //自适应
        frame.setVisible(true); // 显示窗口
        tempTimestamp = System.currentTimeMillis();

        frame.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                origin.x = e.getX();
                origin.y = e.getY();
            }
        });
        frame.addMouseMotionListener(new MouseMotionAdapter() {
            //鼠标拖拽
            @Override
            public void mouseDragged(MouseEvent e) {
                Point p = frame.getLocation();
                frame.setLocation(p.x+e.getX()- origin.x,p.y+e.getY()-origin.y);
            }

            //鼠标移动中心位置显示按钮
            @Override
            public void mouseMoved(MouseEvent e) {
                if ((126<e.getX() && e.getX()<174) && (51<e.getY() && e.getY()<99)){
                    jButton.setVisible(true);
                } else {
                    jButton.setVisible(false);
                }
                System.out.println(e.getX()+":"+e.getY());
            }
        });
        return jLabel;

    }

    public static JProgressBar createJProgressBar(){
        JProgressBar jProgressBar=new JProgressBar(0,100); //创建一个最小值是0，最大值是100的进度条
        jProgressBar.setValue(currentProgress);
        jProgressBar.setBorderPainted(false);
        jProgressBar.setForeground(Color.BLUE);
        jProgressBar.setPreferredSize(new Dimension(layWidth,5));

        // 模拟延时操作进度, 每隔 0.1 秒更新进度
        new Timer(50, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                long add = System.currentTimeMillis() - tempTimestamp;
                double addRate = NumberUtil.div(add*100L,intervalSecond*1000L);
//                System.out.println(add + ":" + addRate);
                currentProgress = (int)addRate;
                if (currentProgress > 100) {
                    currentProgress = 100;
                }
                jProgressBar.setValue(currentProgress);
            }
        }).start();

        jProgressBar.setToolTipText(intervalSecond.toString());

        jProgressBar.addMouseWheelListener(new MouseWheelListener(){
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                int wheelRotation = e.getWheelRotation();
                if (1 == wheelRotation){
                    if (intervalSecond<100){ //设置最大最小间隔时间
                        intervalSecond++;
                    }
                } else if (-1 == wheelRotation){
                    if (intervalSecond>10){
                        intervalSecond--;
                    }
                }
                jProgressBar.setToolTipText(intervalSecond.toString());
            }
        });

        return jProgressBar;
    }

    public static JButton createJButton(){
        JButton jButton = new JButton();
        jButton.setIcon(pause);
        jButton.setBounds(126,51, 48,48);
        jButton.setOpaque(false); //设置按钮透明
        jButton.setContentAreaFilled(false); //设置按钮透明
        jButton.setBorder(null); //取消边框
        jButton.setMargin(new Insets(0,0,0,0));
        jButton.setVisible(false);

        jButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                loop = true^loop;
                if (loop){
                    jButton.setIcon(pause);
                } else {
                    jButton.setIcon(play);
                }

            }
        });
        return jButton;
    }

    public static JFrame createJFrame(){
        JFrame frame = new JFrame("world");
        frame.setUndecorated(true); // 隐藏标题栏
        int w = frame.getToolkit().getScreenSize().width; //屏幕宽度
        frame.setLocation(w-layWidth,0); // 设置启动位置
        frame.setSize(layWidth,layHeight);
        //frame.setBounds(10,10,layWidth-10,layHeight-10); // setLocation和setSize结合体
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setOpacity(0.382f); // 设置透明度
        frame.setAlwaysOnTop(true);// 设置置顶
        try {
            frame.setIconImage(ImageIO.read(ResourceUtil.getStream("play.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return frame;
    }

    public static BorderLayout createLayout(){
        BorderLayout borderLayout = new BorderLayout();
        return borderLayout;
    }

    public static JLabel createJLabel(){
        final JLabel label = new JLabel();
        label.setText(WordUtil.generateText(wordInfoList.get(0)));
        Map<TextAttribute, Object> fontAttr = new HashMap();
        fontAttr.put(TextAttribute.FAMILY, "微软雅黑 Light"); // 字体名称
        fontAttr.put(TextAttribute.SIZE, 12);// 字体大小
//        fontAttr.put(TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE);// 斜体
//        fontAttr.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);// 粗体
//        fontAttr.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);// 下滑线
        Font font = new Font(fontAttr);
        label.setFont(font);
        label.setBackground(new Color(0,0,0,0)); //组件透明
        label.setBackground(Color.BLACK);
        label.setHorizontalAlignment(JLabel.LEFT);
        label.setVerticalAlignment(JLabel.TOP);
        return label;
    }

    public static void main(String[] args) throws UnsupportedLookAndFeelException{
        final JLabel andShowGUI = createAndShowGUI();
        new Runnable(){
            @Override
            public void run() {
                try {
                    int index = 0;
                    while (index < wordInfoList.size()){
                        if (loop){
                            index++;
                        }
                        andShowGUI.setText(WordUtil.generateText(wordInfoList.get(index)));
                        andShowGUI.repaint();
                        currentProgress = 0;
                        tempTimestamp = System.currentTimeMillis();
                        if (index == wordInfoList.size()-1 ){
                            index = 0;
                        }
                        Thread.sleep(intervalSecond*1000L);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.run();

    }


}
