package pojo;

/**
 * @ClassName WordInfo
 * @Author 19012511
 * @Date 2021/5/28 9:23
 **/
public class WordInfo {
    private String word;
    private String phoneticSymbol;
    private String conciseDict;

    public WordInfo() {
    }

    public WordInfo(String word, String phoneticSymbol, String conciseDict) {
        this.word = word;
        this.phoneticSymbol = phoneticSymbol;
        this.conciseDict = conciseDict;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getPhoneticSymbol() {
        return phoneticSymbol;
    }

    public void setPhoneticSymbol(String phoneticSymbol) {
        this.phoneticSymbol = phoneticSymbol;
    }

    public String getConciseDict() {
        return conciseDict;
    }

    public void setConciseDict(String conciseDict) {
        this.conciseDict = conciseDict;
    }
}
