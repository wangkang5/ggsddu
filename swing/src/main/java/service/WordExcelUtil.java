package service;

import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.poi.excel.ExcelReader;

import java.util.List;

/**
 * @ClassName ExcelUtil
 * @Author 19012511
 * @Date 2021/5/28 9:35
 **/
public class WordExcelUtil {

    private final static String excelName = "wordlist.xlsx";

    private static final WordExcelUtil instance = new WordExcelUtil();

    private List<List<Object>> excelInfo;

    private WordExcelUtil() {
        ExcelReader reader = cn.hutool.poi.excel.ExcelUtil.getReader(ResourceUtil.getStream(excelName),0);
        excelInfo = reader.read();
    }

    public static WordExcelUtil getInstance (){
        return instance;
    }

    public List<List<Object>> getExcelInfo() {
        return excelInfo;
    }
}
