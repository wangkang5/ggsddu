package service;

import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import pojo.WordInfo;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName WordUtil
 * @Author 19012511
 * @Date 2021/5/28 9:27
 **/
public class WordUtil {

    private final static String textTemplate = "<html><font size='4' face='微软雅黑'><b>%s</b><br/>%s<br/>%s</font></html>";

    /**
     * 展示start ~ end行数据
     * @param start
     * @param end
     * @return
     */
    public static List<WordInfo> getWordInfoList(Integer start, Integer end){
        List<List<Object>> excelInfo = WordExcelUtil.getInstance().getExcelInfo();
        if (null == start){
            start = 1;
        }
        if (null == end || end.compareTo(excelInfo.size())>0 ){
            end = excelInfo.size();
        }

        List<List<Object>> displayRange = excelInfo.subList(start-1,end);
        List<WordInfo> result = new ArrayList<WordInfo>();
        for (List<Object> objects : displayRange) {
            if (objects.size()<3){
                //todo excel信息不全，在线查找并补全
                continue;
            }
            result.add(new WordInfo(String.valueOf(objects.get(0)),String.valueOf(objects.get(1)),String.valueOf(objects.get(2))));
        }
        return result;
    }

    public static String generateText(WordInfo wordInfo){
        return String.format(textTemplate,wordInfo.getWord(),wordInfo.getPhoneticSymbol(), wordInfo.getConciseDict().replaceAll("\n","<br/>"));
    }

    public static void main(String[] args) throws Exception {
        JFrame frame=new JFrame("Java按钮组件示例");    //创建Frame窗口
        frame.setSize(400, 200);
        JPanel jp=new JPanel();    //创建JPanel对象

        JButton btn1 = new JButton();
//        ImageIcon icon = new ImageIcon("D:\\workspace\\ggsddu\\swing\\src\\main\\resources\\play.png");
        ImageIcon icon = new ImageIcon(ImageIO.read(ResourceUtil.getStream("play.png")));
        btn1.setIcon(icon);

        JButton btn2=new JButton("我是带背景颜色按钮");
        JButton btn3=new JButton("我是不可用按钮");
        JButton btn4=new JButton("我是底部对齐按钮");
        jp.add(btn1);
        btn2.setBackground(Color.YELLOW);    //设置按钮背景色
        jp.add(btn2);
        btn3.setEnabled(false);    //设置按钮不可用
        jp.add(btn3);
        Dimension preferredSize=new Dimension(160, 60);    //设置尺寸
        btn4.setPreferredSize(preferredSize);    //设置按钮大小
        btn4.setVerticalAlignment(SwingConstants.BOTTOM);    //设置按钮垂直对齐方式
        jp.add(btn4);
        frame.add(jp);
        frame.setBounds(300, 200, 600, 300);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
