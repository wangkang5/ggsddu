package model;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import processor.QuwordWPageModelPipeline;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.example.BaiduBaike;
import us.codecraft.webmagic.model.OOSpider;
import us.codecraft.webmagic.model.annotation.ExtractBy;
import us.codecraft.webmagic.model.annotation.ExtractByUrl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ClassName QuwordW
 * @Author 19012511
 * @Date 2021/5/28 17:39
 **/
//@TargetUrl("https://www.quword.com/w/.+")
public class QuwordW {
    @ExtractBy("//*[@id=\"yd-word\"]/text()")
    private String word;

    @ExtractBy("//*[@id=\"yd-word-meaning\"]/ul/li/text()")
    private List<String> ciyi;

    @ExtractBy("//*[@id=\"yd-word-pron\"]/text()")
    private List<String> phoneticSymbol;

    @ExtractBy("//*[@id=\"yd-word-info\"]/div[4]/span/text()")
    private List<String> cikuList;
    @ExtractBy("//*[@id=\"yd-content\"]/div[3]/text()")
    private String memoryMethod;
//    @ExtractBy("//*[@id=\"yd-ciyuan\"]/p/text()")
//    private String ciyuan;
    @ExtractBy("//*[@id=\"yd-liju\"]/dl/dt/text()")
    private List<String> enEgList;
    @ExtractBy("//*[@id=\"yd-liju\"]/dl/dd/text()")
    private List<String> zhEgList;

    @ExtractByUrl
    private String url;

    public String getWord() {
        return word;
    }

    public List<String> getCiyi() {
        return ciyi;
    }

    public List<String> getPhoneticSymbol() {
        return phoneticSymbol;
    }

    public List<String> getCikuList() {
        return cikuList;
    }

    public String getMemoryMethod() {
        return memoryMethod;
    }

    public List<String> getEnEgList() {
        return enEgList;
    }

    public List<String> getZhEgList() {
        return zhEgList;
    }

    public String getUrl() {
        return url;
    }

    public static void main(String[] args) throws Exception {
        String queryUrl = "https://www.quword.com/w/%s";
        //默认UTF-8编码，可以在构造中传入第二个参数做为编码
        FileReader fileReader = new FileReader("E:\\temp\\txt\\merge.txt");
        List<String> wordList = fileReader.readLines();
        List<String> wordUrlList = wordList.stream().map(w -> String.format(queryUrl, w)).collect(Collectors.toList());

        OOSpider ooSpider = OOSpider.create(Site.me().setSleepTime(10), QuwordW.class);
        List<QuwordW> resultItemses = ooSpider.getAll(wordUrlList);



        ArrayList<Map<String, Object>> rows = CollUtil.newArrayList();
        resultItemses.forEach(r->{
            Map<String, Object> temp = new LinkedHashMap<>();
            temp.put("单词",r.getWord());
            temp.put("音标",r.getPhoneticSymbol());
            StringBuilder shiyi = new StringBuilder();
            for (int i = 0; i<r.getCiyi().size(); i++){
                shiyi.append(r.getCiyi().get(i)).append(StrUtil.C_LF);
            }
            temp.put("释义",StrUtil.subBefore(shiyi.toString(),StrUtil.C_LF,true));
            StringBuilder liju = new StringBuilder();
            for (int i = 0; i<r.getEnEgList().size(); i++){
                liju.append(r.getEnEgList().get(i)).append(StrUtil.C_LF).append(r.getZhEgList().get(i)).append(StrUtil.C_LF);
            }
            temp.put("例句",StrUtil.subBefore(liju.toString(),StrUtil.C_LF,true));
            rows.add(temp);
        });

        ExcelWriter writer = ExcelUtil.getWriter("E:\\temp\\txt\\writeTest.xlsx");
        writer.write(rows, true);
        writer.close();

        ooSpider.close();


    }
}
